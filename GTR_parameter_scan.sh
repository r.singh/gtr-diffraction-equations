
#!/bin/bash

resDeg=1.0 # Be careful with spaces in variable declaration

frequencyStart=0.05 # GHz
frequencyStop=5.0
frequencyRes=0.05

holeSizeStart=0.0 # m
holeSizeStop=0.015
holeSizeRes=0.025

targetSizeStart=0.2  # m
targetSizeStop=1.2
targetSizeRes=0.2

monitorDistanceStart=10.0 # m # Acts as reference lambda
#monitorDistanceStop=12.0
#monitorDistanceRes=5.0


beta=(0.05 0.15 0.3 0.6 0.9 0.99 0.999 0.9999)

#./configTable.py ${beta[4]} $holeSizeStart $holeSizeStop $holeSizeRes $targetSizeStart $targetSizeStop $targetSizeRes \
# $monitorDistanceStart $monitorDistanceStop $monitorDistanceRes $frequencyStart $frequencyStop $frequencyRes

#./configScale.py ${beta[1]} $holeSizeStart $holeSizeStop $holeSizeRes $targetSizeStart $targetSizeStop $targetSizeRes \
#$monitorDistanceStart $monitorDistanceStop $monitorDistanceRes $frequencyStart $frequencyStop $frequencyRes

./configScale.py ${beta[4]} $holeSizeStart $holeSizeStop $holeSizeRes $targetSizeStart $targetSizeStop $targetSizeRes \
$monitorDistanceStart $frequencyStart $frequencyStop $frequencyRes

sleep 5

# Total number of entried in the config Table
value=$(<count.txt)
echo $value
#SBATCH --time=8:00:00

# Call the slurm array
sbatch --time 8:00:00 --array 0-$value diffraction_integrals_array_cluster_script.py $resDeg

#sbatch -p long --array 0-$value diffraction_integrals_array_script.py $resDeg

#mv configFile.txt configFile+$date ## cant do that 
