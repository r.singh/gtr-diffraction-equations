#!/bin/bash
resDeg=3 # Be careful with spaces in variable declaration
frequencyRange=2
targetSizeRange=2
holeSizeRange=2
monitorDistanceRange=2

beta=(0.15 0.3 0.6 0.9 0.99)

for betaf in {4..4};
do echo Beta is ${beta[$betaf]};

for ((i=1; i < $frequencyRange;i++));# It appears only Integer multipliction allowed, every unit corresponds to 0.1 MHz
do frequency[i]=$((i));
echo Frequency is ${frequency[$i]}

for ((k=0;k <$holeSizeRange;k++));
do holeSize[k]=$(($k)); # Every unit corresponds to 0.001 m (1mm) # Hole size increment
echo Holesize is ${holeSize[k]}

for j in $(eval echo "{1..$targetSizeRange}");
do targetSize[j]=$(($j)); # Every unit corresponds to 0.05 m (5 cm) # Transverse target size increment
echo "Targetsize is ${targetSize[$j]}"


for ((l=1;l< $monitorDistanceRange;l++));
do monitorDistance[l]=$(($l));
echo Monitor Distance is ${monitorDistance[l]}

sbatch diffraction_integrals_Fiorito_script.py ${beta[$betaf]} ${frequency[$i]} ${holeSize[$k]} ${targetSize[$j]} ${monitorDistance[$l]} $resDeg

done
done
done 
done 
done 
