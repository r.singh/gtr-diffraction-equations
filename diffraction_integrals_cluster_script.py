#!/lustre/lobi/rsingh/miniconda3/envs/EMenv/bin/python

#### Normal incidence radiation (NIR) approximation 



import numpy as np
from itertools import product
from scipy.integrate import dblquad
import cmath as math
from scipy.special import kv
import pandas as pd
import sys




def integrand_x(rho_s,phi_s,omega,pos,q,alpha,gamma,beta,c,imag):
    
    
    Rs = np.sqrt(pos[0]**2+pos[2]**2+rho_s**2+2*pos[0]*rho_s*np.cos(phi_s))
    k = omega/c
    Efx= (q*alpha/(np.pi*beta*c))*kv(1,alpha*rho_s)*rho_s*np.cos(phi_s)*(math.exp(1j*k*Rs)/(Rs**3))*(1-1j*k*Rs)*pos[2]
    if imag == 0:
        return ((1e28/2*np.pi)*Efx.real)
    else:
        return ((1e28/2*np.pi)*Efx.imag)        


def integrand_z(rho_s,phi_s,omega,pos,q,alpha,gamma,beta,c,imag):

    Rs = np.sqrt(pos[0]**2+pos[2]**2+rho_s**2+2*pos[0]*rho_s*np.cos(phi_s))
    k = omega/c
    Efz= (q*alpha/(np.pi*beta*c))*kv(1,alpha*rho_s)*rho_s*np.cos(phi_s)*(math.exp(1j*k*Rs)/(Rs**3))*(1-1j*k*Rs)*(-pos[0]-rho_s*np.cos(phi_s))
    
    if imag == 0:
        return ((1e28/2*np.pi)*Efz.real)
    else:
        return ((1e28/2*np.pi)*Efz.imag)        


def main(argv):

    
    import os
    task_id = int(os.getenv('SLURM_ARRAY_TASK_ID'))
    c= 2.99e8
    mu = 4*np.pi*1e-7
    q = 1.6e-19#/(4*np.pi*8.85e-12) ##q/4pie
    print ('Number of arguments:', len(sys.argv), 'arguments.')
    print ('Argument List:', str(sys.argv))
    configRead=pd.read_csv('/lustre/lobi/rsingh/Python/configFile.txt')
    beta = float(configRead.loc[task_id,'beta'])
    gamma = 1/(np.sqrt(1-beta**2))
    print ("Relativisitic Gamma is ", gamma)
    
    frequency = float(configRead.loc[task_id,'frequency'])
    omega = frequency*1e9*2*np.pi   
    print ("Frequency is ",frequency," GHz")    
    
    hole_size = float(configRead.loc[task_id,'holeSize']) 
    target_size = float(configRead.loc[task_id,'targetSize'])
    r = float(configRead.loc[task_id,'monitorDistance'])                       
    print ("Distance to monitor ",hole_size,target_size,r)    

    # Electric Source vector
    alpha = omega/(beta*c*gamma)  
    print ("Alpha parameter", alpha)
    res_deg = float(sys.argv[1])
    # rho_s=np.zeros((int(rho_s_max/gran_rho)+1),dtype=float)
    gran_phi = res_deg/90 # 1/gran_phi detectors between 0 and pi/2
    phi_ss=np.zeros((int(1/gran_phi)),dtype=float)
    Ex=np.zeros((int(1/gran_phi),2),dtype=float)
    Ez=np.zeros((int(1/gran_phi),2),dtype=float)
    limita = 1e-15    
    for i in range(0,(int(1/gran_phi))):
         phi_ss[i] = i*(np.pi/2)*gran_phi
         rhon=r*np.sin(phi_ss[i])
         zn =r*np.cos(phi_ss[i])
         pos = np.array((rhon,0.0,zn)) # Position of field calculation
         Ex[i,0]=dblquad(integrand_x,0,2*np.pi, lambda phi_s: hole_size, lambda phi_s: target_size,args=(omega,pos,q,alpha,gamma,beta,c,0),epsabs=limita)[0]
         Ex[i,1]=dblquad(integrand_x,0,2*np.pi, lambda phi_s: hole_size, lambda phi_s: target_size,args=(omega,pos,q,alpha,gamma,beta,c,1),epsabs=limita)[0]
         Ez[i,0]=dblquad(integrand_z,0,2*np.pi, lambda phi_s: hole_size, lambda phi_s: target_size,args=(omega,pos,q,alpha,gamma,beta,c,0),epsabs=limita)[0]
         Ez[i,1]=dblquad(integrand_z,0,2*np.pi, lambda phi_s: hole_size, lambda phi_s: target_size,args=(omega,pos,q,alpha,gamma,beta,c,1),epsabs=limita)[0]
#         print ("Step ", i+1 ," of scalar" , (int(1/gran_phi)))
    

    fileName = "Beta="+str(beta)+"Freq="+str(frequency)+"MonitorDistance"+str(r)+"TargetSize"+str(target_size)+"HoleSize"+str(hole_size)+"no_approx.npz"
    np.savez("/lustre/lobi/rsingh/Python/Reichert_scalar_paper/"+fileName,x=Ex,z=Ez)
if __name__ == "__main__":
   main(sys.argv[1:])
