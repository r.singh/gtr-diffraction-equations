#!/lustre/lobi/rsingh/miniconda3/envs/EMenv/bin/python

#### Vector electromagnetic theory for the transition and diffraction radiation


from sympy.vector import CoordSys3D,Del
from sympy import diff
#import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import ode as ode
#from matplotlib import cm
from itertools import product
from sympy import symbols, Function, Eq, solve,cos,sin
from scipy.integrate import quad,romberg, dblquad
import cmath as math
from scipy.special import kv
import sys, getopt
import itertools as it



def modulus(vec):
    return math.sqrt(vec.dot(vec))


def integrand(rho_s,phi_s,omega,pos,q,alpha,gamma,beta,c,comp,imag):

    R = CoordSys3D('R')
    O = R.origin
    Ra = O.locate_new('Ra', pos[0]*R.i + pos[1]*R.j + pos[2]*R.k)
    k =(Ra.position_wrt(O).normalize())*(omega/c) ##Multiply by 2*pi/wavelength
    ns = O.locate_new('ns',0*R.i+0*R.j+1*R.k) # normal surface vector
    rs = O.locate_new('rs',rho_s*cos(phi_s)*R.i+rho_s*sin(phi_s)*R.j+0*R.k)
    Rs = (Ra.position_wrt(O)-rs.position_wrt(O))
    ke=k.dot(Rs)
    z = 0 # Source field at target
    Efx= (q*alpha/(np.pi*beta*c))*kv(1,alpha*rho_s)*np.cos(phi_s)*np.exp(1j*(omega/c)*z) # check for ke and z directions
    Efy= (q*alpha/(np.pi*beta*c))*kv(1,alpha*rho_s)*np.sin(phi_s)*np.exp(1j*(omega/c)*z)
    Efz= (-1j/gamma)*(q*alpha/np.pi*beta)*kv(0, alpha*rho_s)*np.exp(1j*(omega/c)*z)

    Efs = Efx*R.i+Efy*R.j+Efz*R.k
    csf = -1*(c/4*np.pi)* Efs.cross(ns.position_wrt(O))

    A=rho_s*(csf*((math.exp(1j*ke)))/modulus(Rs))
    A_mat=(A.to_matrix(R))
    A_ret=np.array(A_mat.tolist()).astype(np.complex)
    
    if imag == 0:
        return ((2/c)*A_ret[comp].real)
    else:
        return ((2/c)*A_ret[comp].imag)        



def main(argv):

    try:
      opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
    except getopt.GetoptError:
        print ('vectorDiffraction.py  <relativistic beta>  <frequency in GHz>  <holesize in m> -<targetsize in m> -<monitor in m>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('vectorDiffraction.py  <beta>  <frequency in units of 0.1 GHz>  <holesize in units of 1mm> -<targetsize in units of 1cm> -<monitor in units of 10cm> <calculation angles in degrees>')
            sys.exit()
    c= 2.99e8
    mu = 4*np.pi*1e-7
    q = 1.6e-19#/(4*np.pi*8.85e-12) ##q/4pie
    print ('Number of arguments:', len(sys.argv), 'arguments.')
    print ('Argument List:', str(sys.argv))
    beta = float(sys.argv[1])
    gamma = 1/(np.sqrt(1-beta**2))
    print ("Relativisitic Gamma is ", gamma)
    
    frequency = float(sys.argv[2])*0.1 # 0.1 to Match with the bash script
    omega = frequency*1e9*2*np.pi   
    print ("Frequency is ",frequency," GHz")    
    
    hole_size = float(sys.argv[3])*0.002 # to match with bash script definition 
    target_size = float(sys.argv[4])*0.2 # match with bash script
    r = float(sys.argv[5])*0.3
                       
    print ("Distance to monitor ",r)    

    # Electric Source vector
    alpha = omega/(beta*c*gamma)
    k_e = omega/beta*c    
    print ("Alpha parameter", alpha)
    res_deg = float(sys.argv[6])
    # rho_s=np.zeros((int(rho_s_max/gran_rho)+1),dtype=float)
    gran_phi = res_deg/90 # 1/gran_phi detectors between 0 and pi/2
    phi_ss=np.zeros((int(1/gran_phi)),dtype=float)
    A1=np.zeros((int(1/gran_phi),2),dtype=float)
    A2=np.zeros((int(1/gran_phi),2),dtype=float)
    A3=np.zeros((int(1/gran_phi),2),dtype=float)
    #A=np.zeros((int(1/gran_phi)),dtype=complex)
    E=np.zeros((int(1/gran_phi),3),dtype=complex)
    
    for i in range(0,(int(1/gran_phi))):
         phi_ss[i] = i*(np.pi/2)*gran_phi
         rhon=r*np.sin(phi_ss[i])
         zn =r*np.cos(phi_ss[i])
         pos = np.array((rhon,0.0,zn)) # Position of field calculation
         A1[i,0]=dblquad(integrand,0,2*np.pi, lambda phi_s: hole_size, lambda phi_s: target_size,args=(omega,pos,q,alpha,gamma,beta,c,0,0))[0]
         A2[i,0]=dblquad(integrand,0,2*np.pi, lambda phi_s: hole_size, lambda phi_s: target_size,args=(omega,pos,q,alpha,gamma,beta,c,1,0))[0]
         A3[i,0]=dblquad(integrand,0,2*np.pi, lambda phi_s: hole_size, lambda phi_s: target_size,args=(omega,pos,q,alpha,gamma,beta,c,2,0))[0]
         A1[i,1]=dblquad(integrand,0,2*np.pi, lambda phi_s: hole_size, lambda phi_s: target_size,args=(omega,pos,q,alpha,gamma,beta,c,0,1))[0]
         A2[i,1]=dblquad(integrand,0,2*np.pi, lambda phi_s: hole_size, lambda phi_s: target_size,args=(omega,pos,q,alpha,gamma,beta,c,1,1))[0]
         A3[i,1]=dblquad(integrand,0,2*np.pi, lambda phi_s: hole_size, lambda phi_s: target_size,args=(omega,pos,q,alpha,gamma,beta,c,2,1))[0]
         print ("Step ", i ," of " , (1/gran_phi))
    
    
    R = CoordSys3D('R')
    O = R.origin
    
    fileName = "Beta="+str(beta)+"Freq="+str(frequency)+"MonitorDistance"+str(r)+"TargetSize"+str(target_size)+"HoleSize"+str(hole_size)
    for i in range(0,(int(1/gran_phi))):
        phi_ss[i] = i*(np.pi/2)*gran_phi
        rhon=r*np.sin(phi_ss[i])
        zn =r*np.cos(phi_ss[i])
        pos = np.array((rhon,0.0,zn)) # Position of field calculation
        A = (A1[i,0]+1j*A1[i,1])*R.i+(A2[i,0]+1j*A2[i,1])*R.j+(A3[i,0]+1j*A3[i,1])*R.k
        Ra = O.locate_new('Ra', pos[0]*R.i + pos[1]*R.j + pos[2]*R.k)
        k =(Ra.position_wrt(O).normalize())*(omega/c)
        E_temp=A.cross(k)
        E_mat=(E_temp.to_matrix(R))
    #    print(np.array(E_mat.tolist()).astype(np.complex))
        E[i,0]=np.array(E_mat.tolist()).astype(np.complex)[:][0]
        E[i,1]=np.array(E_mat.tolist()).astype(np.complex)[:][1]
        E[i,2]=np.array(E_mat.tolist()).astype(np.complex)[:][2]
        np.save("/lustre/lobi/rsingh/Python/"+fileName,E,allow_pickle=True, fix_imports=True)

if __name__ == "__main__":
   main(sys.argv[1:])

