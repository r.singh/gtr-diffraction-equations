***
# GTR Diffraction equations

GTR equations for parameter scan on HPC
These repo implements the equation shown in this [paper](https://journals.aps.org/prab/abstract/10.1103/PhysRevAccelBeams.25.032801).

GTR_parameter_scan.sh provies the range of parameters and runs many copies of script diffraction_equations_cluster_script.py.

```
cd directory_to_clone
git remote add origin https://git.gsi.de/r.singh/gtr-diffraction-equations.git
git branch -M main
git pull -uf origin main
```
## Setting up Python environment on virgo HPC

Here is a quick summary how you can run Python on the HPC cluster. You could use the same template for running code written in other programming languages, however for our purposes, we will only need python.

First step is to go through these links and understand the usage of HPC and slurm.
[Older SLURM](https://wiki.gsi.de/bin/view/Linux/SlurmUsage) and [Current Virgo] (https://hpc.gsi.de/virgo/preface.html)

- [ ] Create a Miniconda environment on lustre

You can use Miniconda (https://docs.conda.io/en/latest/miniconda.html)
to obtain different versions of Python and to create custom environments (where you can install arbitrary packages). You should choose a user-wide install since that doesn't require root privileges. It should be as simple as downloading the ".sh" file from their website and then running "bash Miniconda3-latest-Linux-x86_64.sh" and following the steps through the installation.

- [ ] Create the virtual enviroment with the dependencies 

Then once installed, you can can run "conda create -n myenv python=3.8"
where "myenv" is the name of your environment that you are free to choose. You can activate this environment then via "conda activate myenv". Now the shell prompt should be prefixed with "(myenv) " and anything Python-related that you do, remains within that virtual environment (it adjusts the PATH accordingly). So when you type "python"
it should drop you in the Python 3.8 interpreter that you just installed. Or when you type "pip install whatever" it should install only in that environment, no affecting anything else. Of course you can have multiple such environments in parallel, if you need.

- [ ] Running using the SBATCH command

For running a Python script, there are multiple options. Option A is to use a shebang line at the beginning of your Python script of the form "#!/lustre/lobi/<your-username>/miniconda3/envs/myenv/bin/python"
(replace <your-username> and perhaps "myenv" if you selected a different name). You also need to make this file executable via `chmod +x script.py`. Then you can submit this file as a job using the "sbatch"
command, i.e. "sbatch script.py". This SBATCH supports various configuration options which can be found [here](https://slurm.schedmd.com/sbatch.html). By default it will create log files for stdout and stderr and submit to the "main" partition with max.
runtime of 8 hours (there is also a "long" partition with max. runtime of 7 days).
With option A you can only submit a single Python script. That's no problem, since Python also contains the "subprocess" module which can be used to invoke other stuff like MADX (or cpymad).
However there's also option B which means to put everything into a Bash-file, again with shebang line "#!/bin/bash" and then you can invoke everything from there (including Python). It also supports SBATCH configuration via special comments. So for example if you want to run MADX, you can just download the MADX executable, put it on the cluster and use the following Bash job file:

```
#!/bin/bash

# Task name (this name will be displayed in "squeue" command) #SBATCH -J test

# Run time limit (5 hours)
#SBATCH --time=5:00:00

# Working directory on shared storage (important!) #SBATCH -D /lustre/lobi/<your-username>/<your-folder>

# Execute application code
/lustre/lobi/<your-username>/bin/madx example.madx /lustre/lobi/<your-username>/miniconda3/envs/myenv/bin/python script.py

```

The "#SBATCH" comments are special configuration comments, so you don't need to repeat it when submitting. The actual application code is executed at the bottom (MADX followed by Python; note that the Python executable from the virtual environment is invoked). This way you can simply submit "sbatch job.sh" and it can save all the configuration for you.

Now usually you want to submit multiple similar jobs on the cluster, to perform something in parallel (e.g. particle tracking). This you can do by using the "--array" option for SBATCH. For example `sbatch
--array=0-99 script.py` would submit 100 jobs in parallel, all running the "script.py" script. To distinguish between these different job instances (e.g. for reading configuration or writing output to different files, so they won't overwrite each other), you can refer to the "SLURM_ARRAY_TASK_ID" environment variable. This is set by the job scheduler and it will contain the job numbers (i.e. "0" for the first instance, "1" for the second, and so on until "99") to get this in Python you can do:

import os
task_id = int(os.getenv('SLURM_ARRAY_TASK_ID'))


## Integrate with your tools

- [ ] [Set up project integrations](https://git.gsi.de/r.singh/gtr-diffraction-equations/-/settings/integrations)



