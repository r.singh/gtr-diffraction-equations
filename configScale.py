#!/lustre/lobi/rsingh/miniconda3/envs/EMenv/bin/python
#### Beta scaled config table for slurm

import numpy as np
from itertools import product
import itertools as it
import sys, getopt
import pandas as pd
import argparse

def main(argv):

    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-v", "--verbose", action="store_true")
    group.add_argument("-q", "--quiet", action="store_true")
    parser.add_argument("beta", type=float, nargs=1, help="Relativistic beta")
    parser.add_argument("holeSizeRange", type=float, nargs=3, help="Hole Size scaling [start stop res]")
    parser.add_argument("targetSizeRange", type=float, nargs=3, help="Target Size scaling [start stop res]")
#    parser.add_argument("monitorDistanceRange", type=float, nargs=3, help="Monitor Distance scaling [start stop res]")
    parser.add_argument("monitorDistanceRange", type=float, nargs=1, help="Reference lambda [start]")
    parser.add_argument("frequencyRange", type=float, nargs=3, help="Frequency range [start stop res]")
    args = parser.parse_args()
    beta = args.beta
    print (type(beta[0]))
    gamma = 1/(np.sqrt(1-beta[0]**2))
    frequencyRange = args.frequencyRange
    frequency=np.arange(frequencyRange[0],frequencyRange[1],frequencyRange[2])

    targetSizeRange = args.targetSizeRange
    targetSize=np.arange(targetSizeRange[0],targetSizeRange[1],targetSizeRange[2])*beta[0]*gamma

    holeSizeRange = args.holeSizeRange
    holeSize=np.arange(holeSizeRange[0],holeSizeRange[1],holeSizeRange[2])#*beta[0]*gamma

    monitorDistanceRange = args.monitorDistanceRange
    monitorDistance=np.array((1.0,2.0))*monitorDistanceRange[0]
#    monitorDistance=np.arange(monitorDistanceRange[0],monitorDistanceRange[1],monitorDistanceRange[2])*(beta[0]**2)*(gamma**2)
    targetSize=targetSize#*monitorDistanceRange[0] # NOrmalize to the wavelength
    if args.quiet:
        print("Argument list not printed")
    elif args.verbose:
        print("Relativistic Beta is {}".format(beta))
        print(" Frequency start {} (GHz) to frequency stop {} (GHz) with resolution {} (GHz)".format(frequencyRange[0], frequencyRange[1], frequencyRange[2]))
        print(" Target Size start {} (m) to Target Size stop {} (m) with resolution {} (m)".format(targetSizeRange[0], targetSizeRange[1], targetSizeRange[2]))
        print(" Hole Size start {} (m) to Hole Size stop {} (m) with resolution {} (m)".format(holeSizeRange[0], holeSizeRange[1], holeSizeRange[2]))
        print(" Monitor Distance start {} (m) to Monitor Distance stop {} (m) with resolution {} (m)".format(monitorDistanceRange[0]))#, monitorDistanceRange[1], monitorDistanceRange[2]))
    else:
        print("Relativistic Beta is {}".format(beta))
        print(" Frequency start {} (GHz) to frequency stop {} (GHz) with resolution {} (GHz)".format(frequencyRange[0], frequencyRange[1], frequencyRange[2]))
        print(" Target Size scale start {} (m) to Target Size scale stop {} (m) with scale resolution {} (m)".format(targetSizeRange[0], targetSizeRange[1], targetSizeRange[2]))
        print(" Hole Size scale start {} (m) to Hole Size stop {} (m) with resolution {} (m)".format(holeSizeRange[0], holeSizeRange[1], holeSizeRange[2]))
        print(" Monitor Distance scaled with wavelength {} (m)".format(monitorDistanceRange[0]))#, monitorDistanceRange[1], monitorDistanceRange[2]))
    count = 0
    configWrite = pd.DataFrame({'beta':[],'holeSize':[],'targetSize':[],'monitorDistance':[],'frequency':[]})
    for a in range(len(beta)):
        for i in range(len(holeSize)):
            for j in range(len(targetSize)):
                for k in range(len(monitorDistance)):
                    for l in range(len(frequency)):
                        configWrite.loc[count,'beta'] = beta[a]
                        configWrite.loc[count,'holeSize'] = format(holeSize[i], '.3f')
                        configWrite.loc[count,'targetSize'] = format(targetSize[j], '.2f')
                        configWrite.loc[count,'monitorDistance'] = format(monitorDistance[k], '.2f')
                        configWrite.loc[count,'frequency'] = format(frequency[l], '.2f')
                        count=count+1
                    
    configWrite.to_csv('/lustre/lobi/rsingh/Python/configFile.txt',index=False) 
    return count


if __name__ == "__main__":
   count=main(sys.argv[1:])
   f=open('/lustre/lobi/rsingh/Python/count.txt','w+') 
   f.write(str(count))
   f.close()
